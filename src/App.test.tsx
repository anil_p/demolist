import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import { ContextProvider } from './store/ContextStore';

test('renders learn react link', () => {
  render(
    <ContextProvider>
      <BrowserRouter>
        <App/>
      </BrowserRouter>
    </ContextProvider>
  );
  const linkElement = screen.getByText(/Welcome to Dashboard/i);
  expect(linkElement).toBeInTheDocument();
});
