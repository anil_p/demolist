import React, {ReactNode, createContext, useContext, useState } from 'react';
    
    interface Props {
    children : ReactNode
    }
    
    const StateContext = createContext<any>(null);
    
    export const ContextProvider = ({ children }:Props) => {
        const [favouriteList, setFavouriteList] = useState([]);
        const [scrolledList, setScrolledList] = useState([]);
        const [currentPage, setCurrentPage] = useState(1);
    
        return (
            <StateContext.Provider value={{favouriteList, setFavouriteList, currentPage, setCurrentPage, scrolledList, setScrolledList}}>
                {children}
            </StateContext.Provider>
        )
    }
    
    export const useStateContext = () => useContext(StateContext)