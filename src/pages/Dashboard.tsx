import React, { useState } from 'react';
import { ButtonInput } from '../reusable/ButtonInput';
import { useNavigate } from 'react-router-dom';
import { FavouriteList } from './FavouriteList';
import { Container, Row } from 'react-bootstrap';


export const Dashboard = ()=>{
    const navigate = useNavigate();

    const buttonClick = async()=>{
        navigate('/list')
    }

    return (
        <>
        <Container>
            <h1>welcome to Dashboard</h1>
            <ButtonInput label='Go to list' onClick={buttonClick}/>
            <Row>
                <FavouriteList/>
            </Row>
        </Container>
        </>

    )
}