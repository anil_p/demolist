import React, { Component,ErrorInfo,ReactNode,useState } from 'react';

interface Props{
    children?: ReactNode;
    navigation?: any;
}

interface State{
    hasError: boolean;
}

class ErrorBoundary extends React.Component<Props, State> {
    public state: State = { hasError: false };
    static getDerivedStateFromError(error: Error) {
      // Update state so the next render will show the fallback UI.
      return { hasError: true };
    }
  
    componentDidCatch(error: Error, errorInfo: ErrorInfo) {
      // You can also log the error to an error reporting service
      console.log(error, errorInfo);
    }
  
    render() {
      if (this.state.hasError) {
        // You can render any custom fallback UI
        return <h1>Something went wrong.</h1>;
      }
  
      return this.props.children; 
    }
  }

  export default ErrorBoundary;