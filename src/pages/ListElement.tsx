import React, { useContext, useEffect, useState } from 'react';
import { getList } from '../api/listElement';
import {Container, Row, Col, ListGroup, Card, Button} from 'react-bootstrap';
import { ButtonInput } from '../reusable/ButtonInput';
import { useStateContext } from '../store/ContextStore';
import { useNavigate } from 'react-router-dom';

export const ListElement = ()=>{
    const {favouriteList, setFavouriteList, currentPage, setCurrentPage, scrolledList, setScrolledList} = useStateContext();
    const itemsPerPage = 10;
    const navigate = useNavigate();

    useEffect(() => {
        getListData();
    }, [currentPage]);

    const handleScroll = () => {
        const scrollY = window.scrollY;
        const windowHeight = window.innerHeight;
        const documentHeight = document.documentElement.scrollHeight;
        if (scrollY + windowHeight >= documentHeight - 100) {
          setCurrentPage(currentPage + 1);
        }
    }

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => {
          window.removeEventListener('scroll', handleScroll);
        };
      }, [currentPage]);

    const getListData = async()=>{
        try{
            const result = await getList(currentPage, itemsPerPage);
            const data = result.data;
            setScrolledList([...scrolledList, ...data]);
        }catch(error:any){
            console.log("List page error: ", error);
        }
    }

    const makeFavouriteOrUnFavourite = async(item:any)=>{
        if(favouriteList.findIndex((dt:any)=> dt.id === item.id) !== -1){
            const data = favouriteList.filter((dt:any)=> dt.id !== item.id);
            setFavouriteList(data);
        }else{
            setFavouriteList([...favouriteList, item])
        }
        
    }

    const buttonClick = async()=>{
        navigate('/')
    }

    return (
        <>
            <Container>
                <div style={{marginTop:8}}><ButtonInput label='Go to Dashboard' onClick={buttonClick}/></div>
                <h1>Display List items:</h1>
                <Row xs={1} md={2} className="g-4">
                    {scrolledList.map((item:any, idx:any) => (
                        <Col key={idx} xs={6} md={4} sm={4}>
                        <Card>
                        <Card.Img variant="top" src={item.thumbnailUrl}/>
                        <Card.Body>
                            <Card.Title>Id: {item.id}</Card.Title>
                            <Card.Text>
                            Title: {item.title}
                            </Card.Text>
                            <ButtonInput label={favouriteList.findIndex((dt:any)=> dt.id === item.id) !== -1 ? 'UnFavourite': 'Add To Favourite'} disabled={false} onClick={(e=>{makeFavouriteOrUnFavourite(item)})}/>
                        </Card.Body>
                        </Card>
                        </Col>
                    ))}
                </Row>
                
            </Container>
        </>
    )
}