export interface IUser{
    name: string;
    lastName:string;
    email: string;
    _id?: string;
}