import React, { useState } from 'react';
import { Button, Container, Form, Row } from 'react-bootstrap';
import {IUser} from './IUser';
import { useNavigate } from 'react-router-dom';
import { createUser } from '../../api/user.service';



const defaultUser = {
    name:'',
    lastName:'',
    email:''
}

const defaultErrorMessages = {
    name:{toggle:false, message:''},
    lastName:{toggle:false, message:''},
    email:{toggle:false, message:''}
}

export const User = ()=>{
    const [user, setUser] = useState<IUser>(defaultUser);
    const [errorMessages, setErrorMessages] = useState(defaultErrorMessages);
    const navigate = useNavigate();
    
    const changeInput = (value:string, type: string)=>{
        console.log(value, type);
        setUser({
            ...user,
            [type]: value
        })
    }

    const validateInputs = () =>{
        let check = true;
        setErrorMessages(defaultErrorMessages);
        Object.keys(user).map((key)=>{
            if(!Boolean(user['name'])){
                check = false;
                setErrorMessages((prev)=>{
                    return {
                        ...prev,
                        name:{
                            toggle: true,
                            message: 'First name is required'
                        }
                    }
                })
            }
            if(!Boolean(user['lastName'])){
                check = false;
                setErrorMessages((prev)=>{
                    return {
                        ...prev,
                        lastName:{
                            toggle: true,
                            message: 'Last name is required'
                        }
                    }
                })
            }
            if(!Boolean(user['email'])){
                check = false;
                setErrorMessages((prev)=>{
                    return {
                        ...prev,
                        email:{
                            toggle: true,
                            message: 'Email is required'
                        }
                    }
                })
            }
        })
        return check;
          
      }

    const submitForm = async()=>{
        try{
            console.log(user, validateInputs());
            if(!validateInputs()){
                console.log("input values are required")
                return;
            }
            const result = await createUser(user);
            console.log('user response: ', result)
            navigate('/userlist');
        }catch(err:any){
            console.log(err)
        }
    }
    const back = async()=>{
        navigate('/userlist')
    }

    return (
        <>
        <Container>
            <br />
            <h3>Create User</h3>
            <Row>
                <Form>
                    <Form.Group className="mb-3" controlId="formBasicName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter First Name"  value={user.name} onChange={(e)=>changeInput(e.target.value, 'name')} required/>
                        { errorMessages['name'].toggle ? <small style={{color:'red'}}>{errorMessages.name.message}</small> : '' }
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicLastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter Last Name" value={user.lastName} onChange={(e)=>changeInput(e.target.value, 'lastName')} required/>
                        { errorMessages['lastName'].toggle ? <small style={{color:'red'}}>{errorMessages.lastName.message}</small> : '' }
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={user.email} onChange={(e)=>changeInput(e.target.value, 'email')} required />
                        { errorMessages['email'].toggle ? <small style={{color:'red'}}>{errorMessages.email.message}</small> : '' }
                    </Form.Group>
                    <Button variant="primary" onClick={submitForm}>
                        Submit
                    </Button>
                    <Button variant="primary" onClick={back} style={{margin:8}}>
                        Back
                    </Button>
                </Form>

            </Row>
        </Container>
        </>

    )
}