import React, { useEffect, useState } from 'react';
import { Button, Container, Form, ListGroup, Row } from 'react-bootstrap';
import {IUser} from './IUser';
import { getUsers } from '../../api/user.service';
import { useNavigate } from 'react-router-dom';

export const UserList = ()=>{
    const [userList, setUserList] = useState<IUser[]>([]);
    const navigate = useNavigate();
    
    useEffect(() => {
        getUserList();
    }, []);

    const getUserList = async()=>{
        try{
            const result = await getUsers();
            const data = result.data;
            console.log(data['userData'])
            setUserList([...userList, ...data['userData']])
        }catch(err:any){
            console.log(err)
        }
        
    }

    const buttonClick = async()=>{
        navigate('/user')
    }

    if(!userList.length){
        return (
            <>
            <Container>
                <Row>
                    <br />
                    <h4>No user found</h4>
                    <Button onClick={buttonClick}>Create New User</Button>
                </Row>
            </Container>
            </>
        )
    }

    return (
        <>
        <Container>
            <br />
            <h3>User list:</h3>
            <Row>
            <Button onClick={buttonClick}>Create New User</Button>
                <ListGroup>
                    {userList.map((key:IUser)=>{
                        return <>
                            <ListGroup.Item key={key._id}>{key.name} {key.lastName} ({key.email})</ListGroup.Item>
                        </>
                    })}
                    
                </ListGroup>
            </Row>
        </Container>
        </>

    )
}