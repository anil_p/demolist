import React, { useState } from 'react';
import { Card, Col, Container, ListGroup, Row } from 'react-bootstrap';
import { ButtonInput } from '../reusable/ButtonInput';
import { useStateContext } from '../store/ContextStore';

interface Item{
    id: string;
    title:string;
    thumbnailUrl: any;
}

interface ListProps{
    items: Array<Item>
}

export const FavouriteList = ()=>{
    const {favouriteList} = useStateContext();
    
    return (
        <>
            <Container>
                <h1>Display List items:</h1>
                <Row xs={1} md={2} className="g-4">
                    {favouriteList.map((item:any, idx:any) => (
                        <Col key={idx} xs={6} md={4} sm={4}>
                        <Card>
                        <Card.Img variant="top" src={item.thumbnailUrl}/>
                        <Card.Body>
                            <Card.Title>Id: {item.id}</Card.Title>
                            <Card.Text>
                            Title: {item.title}
                            </Card.Text>
                            {/* <ButtonInput label="Add to favourite" disabled={false} onClick={(e=>{addToFavourite(item)})}/> */}
                        </Card.Body>
                        </Card>
                        </Col>
                    ))}
                </Row>
                
            </Container>
        </>
    )
}