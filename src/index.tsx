import React from 'react';
import ReactDOM from 'react-dom/client';
import './custom.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import { ListElement } from './pages/ListElement';
import { NoPageFound } from './pages/NoPageFound';
import ErrorBoundary from './pages/ErrorBoundary/ErrorBoundary';
import { Dashboard } from './pages/Dashboard';
import { ContextProvider } from './store/ContextStore';
import { User } from './pages/User/User';
import { UserList } from './pages/User/UserList';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <ErrorBoundary>
      <ContextProvider>
        <BrowserRouter>
          <Routes>
              <Route path='/' element={<App />} />
              <Route path="user" element={<User />} />
              <Route path="userlist" element={<UserList />} />
              <Route path="list" element={<ListElement />} />
              <Route path="*" element={<NoPageFound />} />
          </Routes>
        </BrowserRouter>
      </ContextProvider>
    </ErrorBoundary>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
