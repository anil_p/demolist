import axios from 'axios';
import {IUser} from '../pages/User/IUser';

const url: string = 'http://localhost:4000';

export const createUser = (obj:IUser)=>{
    return axios.post(url+'/user', obj, {headers:{"Content-Type":'application/json'}});
}

export const getUsers = ()=>{
    return axios.get(url+'/user', {headers:{"Content-Type":'application/json'}});
}