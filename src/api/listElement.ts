import axios from 'axios';

export const getList = async(currentPage: number, itemPerPage: number)=>{
    return await axios.get(`https://jsonplaceholder.typicode.com/albums/1/photos?_page=${currentPage}&limit=${itemPerPage}`);
}
