import React from 'react';
import Button from 'react-bootstrap/Button';

interface ButtonProps{
    label: string;
    onClick: (value:any)=> void;
    disabled?: boolean

}

export const ButtonInput = (props: ButtonProps)=>{
    const {label, onClick, disabled} = props;
    return (
        <>
          <Button 
           disabled={disabled}
           onClick={onClick}>
            {label}
           </Button>
        </>
    )
}